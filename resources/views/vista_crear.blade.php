@extends('layouts.app')
@section('content')
    <section id="maincontent">
        <div class="container">
            <h1>Crear Convocatoria</h1>
            @if ( $errors->any() )
                <ul>
                    @foreach ($errors->all() as $error)
                        <li> {{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            <form action="{{url('convocatoria')}}" method="post">
                {{ csrf_field()}}
                <label for="nombre">Nombre</label>
                <input type="text" name="nombre" id="nombre" value="{{old('nombre')}}">
                <label for="descripcion">Descripcion</label>
                <input type="text" name="descripcion" id="descripcion">
                <label for="unidad_academica">Unidad Academica</label>
                <input type="text" name="unidad_academica" id="unidad_academica">
                <label for="tipo_convocatoria">Tipo Convocatoria</label>
                <input type="text" name="tipo_convocatoria" id="tipo_convocatoria">
                <button type="submit">Crear Usuario</button>
            </form>
        </div>
    </section>
@endsection