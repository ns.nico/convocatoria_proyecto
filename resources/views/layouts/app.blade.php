<!DOCTYPE html>
<html lang="en">
<head>
  @include('includes.head')
</head>

<body>
    <header>
        @include('includes.navbar')
    </header>
    <div>
        @yield('content')
    </div>
    <footer class="footer">
        @include('includes.footer')
    </footer>

    @include('includes.script')

</body>
</html>
