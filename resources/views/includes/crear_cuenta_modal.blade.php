<!-- Crear cuenta Modal -->
<div id="crearCuenta" class="modal styled hide fade" tabindex="-1" role="dialog" aria-labelledby="mySignupModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="mySignupModalLabel">Crea una <strong>cuenta</strong></h4>
    </div>
    <div class="modal-body">
        <form class="form-horizontal">
            <div class="control-group">
                <label class="control-label" for="inputText">Nombre de usuario</label>
                <div class="controls">
                    <input type="text" id="inputText" placeholder="Usuario">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputEmail">Email</label>
                <div class="controls">
                    <input type="text" id="inputEmail" placeholder="Email">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputSignupPassword">Contraseña</label>
                <div class="controls">
                    <input type="password" id="inputSignupPassword" placeholder="Contraseña">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputSignupPassword2">Confirmar Contraseña</label>
                <div class="controls">
                    <input type="password" id="inputSignupPassword2" placeholder="Contraseña">
                </div>
            </div>
            <div class="control-group">
                <div class="controls ">
                    <button type="submit" class="btn btn-color">Crear cuenta</button>
                </div>
            </div>
            <div class="control-group">
                <p class="aligncenter margintop20">
                    Ya tienes una cuenta? <a href="#iniciarSesion" data-dismiss="modal" aria-hidden="true" data-toggle="modal">Iniciar Sesion</a>
                </p>
            </div>
        </form>
    </div>
</div>