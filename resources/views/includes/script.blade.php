<!-- JavaScript Library Files -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery.easing.js') }}"></script>
<script src="{{ asset('js/google-code-prettify/prettify.js') }}"></script>
<script src="{{ asset('js/modernizr.js') }}"></script>
<script src="{{ asset('js/bootstrap.js') }}"></script>
<script src="{{ asset('js/jquery.elastislide.js') }}"></script>
<script src="{{ asset('js/sequence/sequence.jquery-min.js') }}"></script>
<script src="{{ asset('js/sequence/setting.js') }}"></script>
<script src="{{ asset('js/jquery.prettyPhoto.js') }}"></script>
<script src="{{ asset('js/application.js') }}"></script>
<script src="{{ asset('js/jquery.flexslider.js') }}"></script>
<script src="{{ asset('js/hover/jquery-hover-effect.js') }}"></script>
<script src="{{ asset('js/hover/setting.js') }}"></script>
<script src="{{ asset('js/custom.js') }}"></script>
