<!-- Footer ================================================== -->
<div class="container">
    <div class="row">
        <div class="span4">
            <div class="widget">
                <h5>Paginas relacionadas</h5>
                <ul class="regular">
                    <li><a href="http://websis.umss.edu.bo/" target="_blank">Websis Umss</a></li>
                    <li><a href="http://www.fcyt.umss.edu.bo/" target="_blank">Facultad de Ciencias y Tecnologia</a></li>
                    <li><a href="http://www.cs.umss.edu.bo/" target="_blank">CS Umss Departamento de Informatica-Sistemas</a></li>
                    <li><a href="http://www.umss.edu.bo/" target="_blank">Umss Oficial</a></li>
                </ul>
            </div>
        </div>
        <div class="span4">
            <div class="widget">
                <h5>Otras busquedas</h5>
                <ul class="regular">
                    <li><a href="http://websis.umss.edu.bo/serv_estudiantes.asp" target="_blank">Estudiantes Umss Web</a></li>
                    <li><a href="http://moodle3.umss.edu.bo/" target="_blank">Moodle3 Umss</a></li>
                    <li><a href="https://www.facebook.com/UmssBolOficial/" target="_blank">Facebook Umss</a></li>
                </ul>
            </div>
        </div>
        <div class="span4">
            <div class="widget">
                <!-- logo -->
                <a class="brand logo" href="{{ url('/') }}">
					<img src="{{ asset('img/logo-dark.png') }}" alt="">
				</a>
                <!-- end logo -->
                <address>
					<strong>Empresa Registrada, S.R.L</strong><br>
                            Antezana entre La Paz y Oruro<br>
							Edificio Torre Atlanta piso 6, Calle Rafael Antezana, La Paz<br>
							<abbr title="Phone">Tel:</abbr> (591) 956-789-44
				</address>
            </div>
        </div>
    </div>
</div>
<div class="verybottom">
    <div class="container">
        <div class="row">
            <div class="span6">
                <p>
                    &copy; Convocatorias - Todos los derechos reservados
                </p>
            </div>
            <div class="span6">
                <div class="credits">
                Desarrollado por: <a href="">Neosoft Technologies</a>
                </div>
            </div>
        </div>
    </div>
</div>
