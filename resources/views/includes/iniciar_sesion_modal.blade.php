<!-- Iniciar sesion Modal -->
<div id="iniciarSesion" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="mySigninModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="mySigninModalLabel">Ingrese a su <strong>cuenta</strong></h3>
    </div>
    <div class="modal-body">
        <form class="form-horizontal">
            <div class="control-group">
                <label class="control-label" for="inputText">Usuario</label>
                <div class="controls">
                    <input type="text" id="inputText" placeholder="Usuario">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputSigninPassword">Contraseña</label>
                <div class="controls">
                    <input type="password" id="inputSigninPassword" placeholder="Contraseña">
                </div>
            </div>
            <div class="control-group">
                <div class="controls mb-5">
                    <button type="submit" class="btn btn-color">Iniciar Sesion</button>
                </div>
            </div>
            <div class="control-group">
                <p class="aligncenter margintop20">
                    No tienes una cuenta? <a href="#crearCuenta" data-dismiss="modal" aria-hidden="true" data-toggle="modal">Crear Cuenta</a>
                </p>
            </div>
        </form>
    </div>
</div>