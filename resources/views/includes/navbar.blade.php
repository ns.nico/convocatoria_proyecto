@include('includes.iniciar_sesion_modal')
@include('includes.crear_cuenta_modal')
<!-- Navbar================================================== -->
<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <!-- logo -->
            <a class="brand logo" href="{{ url('/') }}"><img src="{{ asset('img/logo.png') }}" alt=""></a>
            <!-- end logo -->
            <!-- top menu -->
            <div class="navigation">
                <nav>
                    <ul class="nav topnav">
                        <li class="dropdown active">
                            <a href="{{ url('/') }}">Inicio</a>
                        </li>
                        <li>
                            <a href="#">Contacto</a>
                        </li>
                        <li>
                            <a href="#iniciarSesion" data-toggle="modal">Iniciar Sesion</a>
                        </li>
                        <!-- no se muy bien que otras pestanhas anhadir de momento -->
                    </ul>
                </nav>
            </div>
            <!-- end menu -->
        </div>
    </div>
</div>