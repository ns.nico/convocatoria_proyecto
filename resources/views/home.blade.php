@extends('layouts.app')
@section('content')
    <section id="intro">
        <div class="jumbotron masthead">
            <div class="container">
                <!-- slider navigation -->
                <div class="sequence-nav">
                    <div class="prev">
                        <span></span>
                    </div>
                    <div class="next">
                        <span></span>
                    </div>
                </div>
                <!-- end slider navigation -->
                <div class="row">
                    <div class="span12">
                        <div id="slider_holder">
                            <div id="sequence">
                                <ul>
                                    <!-- Layer 1 -->
                                    <li>
                                        <div class="info animate-in">
                                            <h2>Convocatorias</h2><br>
                                            <h3>Publicacion de Convocatorias</h3>
                                            <p>
                                                Convocatorias de las distintas Unidades Academicas de la UMMS, para la provision de auxiliares.
                                            </p>
                                            <a class="btn btn-success" href="#">Leer mas &raquo;</a>
                                        </div>
                                        <img class="slider_img animate-in" src="{{ asset('img/slides/img-1.png') }}" alt="">
                                    </li>
                                    <!-- Layer 2 -->
                                    <li>
                                        <div class="info">
                                            <h2>Postulantes</h2><br>
                                            <h3>Registro de postulantes</h3>
                                            <p>
                                                Los postulantes podran registrarse a un item especifico de una convocatoria, 
                                                presentando los documentos especificados en la convocatoria.
                                            </p>
                                            <a class="btn btn-success" href="#">Leer mas &raquo;</a>
                                        </div>
                                        <img class="slider_img" src="{{ asset('img/slides/img-2.png') }}" alt="">
                                    </li>
                                    <!-- Layer 3 -->
                                    <li>
                                        <div class="info">
                                            <h2>Postulantes</h2><br>
                                            <h3>Habilitacion de postulantes</h3>
                                            <p>
                                                Resultados de los postulantes habilitados o inhabilitados de acuerdo a los criterios 
                                                especificados en la convocatoria.
                                            </p>
                                            <a class="btn btn-success" href="#">Leer mas &raquo;</a>
                                        </div>
                                        <img class="slider_img" src="{{ asset('img/slides/img-3.png') }}" alt="">
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- Sequence Slider::END-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="maincontent">
        <div class="container">
            <div class="row">
                <div class="span4 features">
                    <i class="icon-circled icon-32 icon-book left"></i>
                    <h4>Lista de Habilitados/Inhabilitados</h4>
                    <div class="dotted_line">
                    </div>
                    <div class="thumbnail">
                        <div class="image-wrapp">
                            <img src="{{ asset('img/noticias/noticia-1.png') }}" alt="" title="" />
                        </div>
                    </div>
                    <p class="left">
                        Se informa a los postulantes que ya esta la lista de habilitados e inhabilitados para la convocatoria
                        de auxiliatura de docencia correspondiente a la gestion 2020 ...
                    </p>
                    <a href="#">Leer mas</a>
                </div>
                <div class="span4 features">
                    <i class="icon-circled icon-32 icon-book left"></i>
                    <h4>Evaluacion de conocimientos</h4>
                    <div class="dotted_line">
                    </div>
                    <div class="thumbnail">
                        <div class="image-wrapp">
                            <img src="{{ asset('img/noticias/noticia-2.png') }}" alt="" title="" />
                        </div>
                    </div>
                    <p class="left">
                        Se informa a los postulantes que ya esta la lista de habilitados e inhabilitados para la convocatoria
                        de auxiliatura de docencia correspondiente a la gestion 2020 ...
                    </p>
                    <a href="#">Leer mas</a>
                </div>
                <div class="span4 features">
                    <i class="icon-circled icon-32 icon-book left"></i>
                    <h4>Reunion de aclaracion</h4>
                    <div class="dotted_line">
                    </div>
                    <div class="thumbnail">
                        <div class="image-wrapp">
                            <img src="{{ asset('img/noticias/noticia-3.png') }}" alt="" title="" />
                        </div>
                    </div>
                    <p class="left">
                        El dia jueves 5 de marzo de 2020 a horas 09:00 en auditorio del Centro MEMI se realizara una reunion
                        de aclaracion por parte de los consultores TIS. Mayor abundamiento de info...
                    </p>
                    <a href="#">Leer mas</a>
                </div>
            </div>
            <div class="row">
                <div class="pagination pagination-centered">
                    <ul class="pager">
                        <li class="disabled"><a href="#">&laquo;</a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">&raquo;</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="span12">
                    <div class="tagline centered">
                        <div class="row">
                            <div class="span12">
                                <div class="tagline_text">
                                    <h2>Te interesa postularte a algun item de las convocatorias</h2>
                                    <p>
                                        Informate sobre las los requisitos, fechas, requerimientos sobre las distintas convocatorias 
                                    </p>
                                </div>
                                <div class="btn-toolbar cta">
                                    <a class="btn btn-large btn-color" href="#">
                                            <i class="icon-eye-open icon-white"></i> Ver convocatorias </a>
                                    <a class="btn btn-large btn-inverse" href="#">
                                            <i class="icon-star icon-white"></i> Postularse </a>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- end tagline -->
                </div>
            </div>
        </div>
    </section>
@endsection