<?php

namespace App\Http\Controllers;

use App\Http\Requests\ConvocatoriaRequest;
use App\Models\Convocatoria;


class ConvocatoriaController extends Controller
{
    //
    public function vista(){
        return view('vista_crear');
    }
    public function crear(ConvocatoriaRequest $request){
        $data = $request->all();

        Convocatoria::create([
            'nombre'=>$data['nombre'],
            'descripcion'=>$data['descripcion'],
            'unidad_academica_id'=>(int)$data['unidad_academica'],
            'tipo_convocatoria_id'=>(int)$data['tipo_convocatoria']
        ]);

        return redirect('home');
    }
}
