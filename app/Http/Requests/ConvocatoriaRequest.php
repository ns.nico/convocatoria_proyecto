<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ConvocatoriaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected $redirect = 'convocatoria';

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'nombre' => 'required|unique:convocatoria,nombre',
            'descripcion' => 'required',
            'unidad_academica' => 'required',
            'tipo_convocatoria' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'nombre.required' => 'Se requiere el nombre de la convocatoria.',
            'descripcion.required' => 'Se requiere la descripcion de la convocatoria.',
            'unidad_academica.required' => 'Se requiere la unidad academica a la que pertenece.',
            'tipo_convocatoria.required' => 'Se requiere el tipo de convocatoria.',
            'nombre.unique' => 'El nombre de la convocatoria ya ha sido usado.',
        ];
    }
}
