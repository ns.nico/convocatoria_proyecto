<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoConvocatoria extends Model
{
    //
    protected $table = 'tipo_convocatoria';
    public $timestamps = false;
}
