<?php

use App\Models\TipoConvocatoria;
use Illuminate\Database\Seeder;

class TipoConvocatoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        TipoConvocatoria::create([
            'nombre' => 'Auxiliar de Laboratorio',
        ]);

        TipoConvocatoria::create([
            'nombre' => 'Auxiliar de Docencia',
        ]);
    }
}
