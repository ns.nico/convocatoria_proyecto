<?php

use App\Models\Requisito;
use Illuminate\Database\Seeder;

class RequisitoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Requisito::create([
            'nombre' => 'Noveno semestre vencido',
            'descripcion' => 'Todas las materias de noveno semestre vencido'
        ]);
    }
}
