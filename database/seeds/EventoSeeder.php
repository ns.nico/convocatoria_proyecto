<?php

use App\Models\Evento;
use Illuminate\Database\Seeder;

class EventoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Evento::create([
            'nombre' => 'Entrega de documentos',
            'descripcion' => 'Entrega de los documentos requeridos.'
        ]);
    }
}
