<?php

use App\Models\Usuario;
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->calls([
            UnidadAcademicaSeeder::class,
            DocumentoRequeridoSeeder::class,
            EventoSeeder::class,
            RequisitoSeeder::class,
            TipoConvocatoriaSeeder::class,
            AuxiliaturaSeeder::class,
            UsuarioSeeder::class
        ]);
        
    }
    
    protected function calls(array $tables){
        foreach($tables as $table){
            $this->call($table);
        }
    }

}
