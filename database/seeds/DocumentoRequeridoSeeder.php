<?php

use App\Models\DocumentoRequerido;
use Illuminate\Database\Seeder;

class DocumentoRequeridoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //DB::table('documento_requerido')->insert([
        //    'nombre' => 'Carnet de Identidad',
        //    'descripcion' => 'Vigente'
        //]);

        DocumentoRequerido::create([
            'nombre' => 'Carnet de Identidad',
            'descripcion' => 'Vigente'
        ]);

        DocumentoRequerido::create([
            'nombre' => 'Kardex',
            'descripcion' => 'Umss'
        ]);
    }
}
