<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\UnidadAcademica;

class UnidadAcademicaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //DB::table('unidad_academica')->insert([
        //    'nombre' => 'Departamento Informática Sistemas'
        //]);
        UnidadAcademica::create([
            'nombre' => 'Departamento Informática Sistemas',
        ]);

        UnidadAcademica::create([
            'nombre' => 'Departamento Matemáticas'
        ]);

        UnidadAcademica::create([
            'nombre' => 'Departamento Física'
        ]);

        UnidadAcademica::create([
            'nombre' => 'Departamento Biología'
        ]);

        
        
    }
}
