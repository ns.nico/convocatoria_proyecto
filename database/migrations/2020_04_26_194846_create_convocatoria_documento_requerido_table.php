<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConvocatoriaDocumentoRequeridoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('convocatoria_documento_requerido', function(Blueprint $table){
            $table->increments('id');

            $table->unsignedInteger('convocatoria_id');
            $table->foreign('convocatoria_id')->references('id')->on('convocatoria');

            $table->unsignedInteger('documento_requerido_id');
            $table->foreign('documento_requerido_id')->references('id')->on('documento_requerido');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('convocatoria_documento_requerido');
    }
}
