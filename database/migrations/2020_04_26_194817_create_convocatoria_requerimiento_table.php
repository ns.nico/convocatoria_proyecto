<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConvocatoriaRequerimientoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('convocatoria_requerimiento', function(Blueprint $table){
            $table->increments('id');

            $table->unsignedInteger('convocatoria_id');
            $table->foreign('convocatoria_id')->references('id')->on('convocatoria');

            $table->unsignedInteger('requerimiento_id');
            $table->foreign('requerimiento_id')->references('id')->on('requerimiento');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('convocatoria_requerimiento');
    }
}
