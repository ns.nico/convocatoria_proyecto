<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequerimientoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('requerimiento', function(Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('cantidad')->nullable(false);
            $table->unsignedInteger('horas_academicas')->nullable(false);

            $table->unsignedInteger('auxiliatura_id');
            $table->foreign('auxiliatura_id')->references('id')->on('auxiliatura');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('requerimiento');
    }
}
