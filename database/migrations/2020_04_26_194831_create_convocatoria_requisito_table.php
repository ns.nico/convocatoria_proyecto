<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConvocatoriaRequisitoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('convocatoria_requisito', function(Blueprint $table){
            $table->increments('id');
            
            $table->unsignedInteger('convocatoria_id');
            $table->foreign('convocatoria_id')->references('id')->on('convocatoria');

            $table->unsignedInteger('requisito_id');
            $table->foreign('requisito_id')->references('id')->on('requisito');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('convocatoria_requisito');
    }
}
