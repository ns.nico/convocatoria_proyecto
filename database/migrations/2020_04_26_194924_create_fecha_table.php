<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFechaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('fecha', function(Blueprint $table){
            $table->increments('id');
            $table->dateTime('inicio')->nullable(false);
            $table->dateTime('fin')->nullable(false);
            $table->string('gestion')->nullable(false);

            $table->unsignedInteger('convocatoria_id');
            $table->foreign('convocatoria_id')->references('id')->on('convocatoria');

            $table->unsignedInteger('evento_id');
            $table->foreign('evento_id')->references('id')->on('evento');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('fecha');
    }
}
