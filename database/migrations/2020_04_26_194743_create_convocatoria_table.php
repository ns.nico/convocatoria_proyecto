<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConvocatoriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('convocatoria', function(Blueprint $table){
            $table->increments('id');
            $table->string('nombre')->nullable(false);
            $table->string('descripcion');

            $table->unsignedInteger('unidad_academica_id');
            $table->foreign('unidad_academica_id')->references('id')->on('unidad_academica')->onDelete('cascade');

            $table->unsignedInteger('tipo_convocatoria_id');
            $table->foreign('tipo_convocatoria_id')->references('id')->on('tipo_convocatoria');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('convocatoria');
    }
}
